﻿namespace WindowsFormsApplication1
{
    partial class anaSayfa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.DosyaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AyarlarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.YönetimDeğişikliğiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ÇıkışToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.KatSakinleriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.KatMalikleriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OtoparkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AidatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AidatToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.AiadatEkleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AidatBilgilariToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AidatDüzenlemeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GiderlerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GiderMakbuzuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GiderSorgulamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GörevliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GörevliToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.YöneticiListesiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DönemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EkstreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MakbuzlarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AidatMakbuzuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GiderMakbuzuToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.FaturalarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.KasaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BakiyeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SorgulamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borçlandırToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.YardımToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuStrip1
            // 
            this.MenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DosyaToolStripMenuItem,
            this.KatSakinleriToolStripMenuItem,
            this.AidatToolStripMenuItem,
            this.GörevliToolStripMenuItem,
            this.MakbuzlarToolStripMenuItem,
            this.KasaToolStripMenuItem,
            this.YardımToolStripMenuItem});
            this.MenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip1.Name = "MenuStrip1";
            this.MenuStrip1.Size = new System.Drawing.Size(662, 24);
            this.MenuStrip1.TabIndex = 2;
            this.MenuStrip1.Text = "MenuStrip1";
            this.MenuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.MenuStrip1_ItemClicked);
            // 
            // DosyaToolStripMenuItem
            // 
            this.DosyaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AyarlarToolStripMenuItem,
            this.YönetimDeğişikliğiToolStripMenuItem,
            this.ÇıkışToolStripMenuItem});
            this.DosyaToolStripMenuItem.Name = "DosyaToolStripMenuItem";
            this.DosyaToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.DosyaToolStripMenuItem.Text = "Dosya";
            // 
            // AyarlarToolStripMenuItem
            // 
            this.AyarlarToolStripMenuItem.Name = "AyarlarToolStripMenuItem";
            this.AyarlarToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.AyarlarToolStripMenuItem.Text = "Ayarlar";
            this.AyarlarToolStripMenuItem.Click += new System.EventHandler(this.AyarlarToolStripMenuItem_Click);
            // 
            // YönetimDeğişikliğiToolStripMenuItem
            // 
            this.YönetimDeğişikliğiToolStripMenuItem.Name = "YönetimDeğişikliğiToolStripMenuItem";
            this.YönetimDeğişikliğiToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.YönetimDeğişikliğiToolStripMenuItem.Text = "Yönetim Değişikliği";
            // 
            // ÇıkışToolStripMenuItem
            // 
            this.ÇıkışToolStripMenuItem.Name = "ÇıkışToolStripMenuItem";
            this.ÇıkışToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.ÇıkışToolStripMenuItem.Text = "Çıkış";
            // 
            // KatSakinleriToolStripMenuItem
            // 
            this.KatSakinleriToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.KatMalikleriToolStripMenuItem,
            this.OtoparkToolStripMenuItem});
            this.KatSakinleriToolStripMenuItem.Name = "KatSakinleriToolStripMenuItem";
            this.KatSakinleriToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.KatSakinleriToolStripMenuItem.Text = "Kat Sakinleri";
            // 
            // KatMalikleriToolStripMenuItem
            // 
            this.KatMalikleriToolStripMenuItem.Name = "KatMalikleriToolStripMenuItem";
            this.KatMalikleriToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.KatMalikleriToolStripMenuItem.Text = "Kat Malikleri";
            this.KatMalikleriToolStripMenuItem.Click += new System.EventHandler(this.KatMalikleriToolStripMenuItem_Click);
            // 
            // OtoparkToolStripMenuItem
            // 
            this.OtoparkToolStripMenuItem.Name = "OtoparkToolStripMenuItem";
            this.OtoparkToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.OtoparkToolStripMenuItem.Text = "Otopark";
            this.OtoparkToolStripMenuItem.Click += new System.EventHandler(this.OtoparkToolStripMenuItem_Click);
            // 
            // AidatToolStripMenuItem
            // 
            this.AidatToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AidatToolStripMenuItem1,
            this.GiderlerToolStripMenuItem});
            this.AidatToolStripMenuItem.Name = "AidatToolStripMenuItem";
            this.AidatToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.AidatToolStripMenuItem.Text = "Gelir\\Gider";
            // 
            // AidatToolStripMenuItem1
            // 
            this.AidatToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AiadatEkleToolStripMenuItem,
            this.AidatBilgilariToolStripMenuItem,
            this.AidatDüzenlemeToolStripMenuItem});
            this.AidatToolStripMenuItem1.Name = "AidatToolStripMenuItem1";
            this.AidatToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.AidatToolStripMenuItem1.Text = "Aidat ";
            // 
            // AiadatEkleToolStripMenuItem
            // 
            this.AiadatEkleToolStripMenuItem.Name = "AiadatEkleToolStripMenuItem";
            this.AiadatEkleToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.AiadatEkleToolStripMenuItem.Text = "Aiadat Makbuzu";
            // 
            // AidatBilgilariToolStripMenuItem
            // 
            this.AidatBilgilariToolStripMenuItem.Name = "AidatBilgilariToolStripMenuItem";
            this.AidatBilgilariToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.AidatBilgilariToolStripMenuItem.Text = "Aidat Bilgileri";
            // 
            // AidatDüzenlemeToolStripMenuItem
            // 
            this.AidatDüzenlemeToolStripMenuItem.Name = "AidatDüzenlemeToolStripMenuItem";
            this.AidatDüzenlemeToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.AidatDüzenlemeToolStripMenuItem.Text = "Aidat Düzenleme";
            // 
            // GiderlerToolStripMenuItem
            // 
            this.GiderlerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.GiderMakbuzuToolStripMenuItem,
            this.GiderSorgulamaToolStripMenuItem});
            this.GiderlerToolStripMenuItem.Name = "GiderlerToolStripMenuItem";
            this.GiderlerToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.GiderlerToolStripMenuItem.Text = "Giderler";
            // 
            // GiderMakbuzuToolStripMenuItem
            // 
            this.GiderMakbuzuToolStripMenuItem.Name = "GiderMakbuzuToolStripMenuItem";
            this.GiderMakbuzuToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.GiderMakbuzuToolStripMenuItem.Text = "Gider Makbuzu";
            // 
            // GiderSorgulamaToolStripMenuItem
            // 
            this.GiderSorgulamaToolStripMenuItem.Name = "GiderSorgulamaToolStripMenuItem";
            this.GiderSorgulamaToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.GiderSorgulamaToolStripMenuItem.Text = "Gider Sorgulama";
            // 
            // GörevliToolStripMenuItem
            // 
            this.GörevliToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.GörevliToolStripMenuItem1,
            this.YöneticiListesiToolStripMenuItem});
            this.GörevliToolStripMenuItem.Name = "GörevliToolStripMenuItem";
            this.GörevliToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.GörevliToolStripMenuItem.Text = "Görevliler";
            // 
            // GörevliToolStripMenuItem1
            // 
            this.GörevliToolStripMenuItem1.Name = "GörevliToolStripMenuItem1";
            this.GörevliToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.GörevliToolStripMenuItem1.Text = "Görevli ";
            this.GörevliToolStripMenuItem1.Click += new System.EventHandler(this.GörevliToolStripMenuItem1_Click);
            // 
            // YöneticiListesiToolStripMenuItem
            // 
            this.YöneticiListesiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DönemToolStripMenuItem,
            this.EkstreToolStripMenuItem});
            this.YöneticiListesiToolStripMenuItem.Name = "YöneticiListesiToolStripMenuItem";
            this.YöneticiListesiToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.YöneticiListesiToolStripMenuItem.Text = "Yönetici Listesi";
            // 
            // DönemToolStripMenuItem
            // 
            this.DönemToolStripMenuItem.Name = "DönemToolStripMenuItem";
            this.DönemToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.DönemToolStripMenuItem.Text = "Dönem ";
            // 
            // EkstreToolStripMenuItem
            // 
            this.EkstreToolStripMenuItem.Name = "EkstreToolStripMenuItem";
            this.EkstreToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.EkstreToolStripMenuItem.Text = "Ekstre Görüntüleme";
            // 
            // MakbuzlarToolStripMenuItem
            // 
            this.MakbuzlarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AidatMakbuzuToolStripMenuItem,
            this.GiderMakbuzuToolStripMenuItem1,
            this.FaturalarToolStripMenuItem});
            this.MakbuzlarToolStripMenuItem.Name = "MakbuzlarToolStripMenuItem";
            this.MakbuzlarToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.MakbuzlarToolStripMenuItem.Text = "Makbuzlar";
            // 
            // AidatMakbuzuToolStripMenuItem
            // 
            this.AidatMakbuzuToolStripMenuItem.Name = "AidatMakbuzuToolStripMenuItem";
            this.AidatMakbuzuToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.AidatMakbuzuToolStripMenuItem.Text = "Aidat Makbuzu";
            this.AidatMakbuzuToolStripMenuItem.Click += new System.EventHandler(this.AidatMakbuzuToolStripMenuItem_Click);
            // 
            // GiderMakbuzuToolStripMenuItem1
            // 
            this.GiderMakbuzuToolStripMenuItem1.Name = "GiderMakbuzuToolStripMenuItem1";
            this.GiderMakbuzuToolStripMenuItem1.Size = new System.Drawing.Size(154, 22);
            this.GiderMakbuzuToolStripMenuItem1.Text = "Gider Makbuzu";
            // 
            // FaturalarToolStripMenuItem
            // 
            this.FaturalarToolStripMenuItem.Name = "FaturalarToolStripMenuItem";
            this.FaturalarToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.FaturalarToolStripMenuItem.Text = "Faturalar";
            // 
            // KasaToolStripMenuItem
            // 
            this.KasaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BakiyeToolStripMenuItem,
            this.SorgulamaToolStripMenuItem,
            this.borçlandırToolStripMenuItem});
            this.KasaToolStripMenuItem.Name = "KasaToolStripMenuItem";
            this.KasaToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.KasaToolStripMenuItem.Text = "Kasa";
            // 
            // BakiyeToolStripMenuItem
            // 
            this.BakiyeToolStripMenuItem.Name = "BakiyeToolStripMenuItem";
            this.BakiyeToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.BakiyeToolStripMenuItem.Text = "Bakiye";
            // 
            // SorgulamaToolStripMenuItem
            // 
            this.SorgulamaToolStripMenuItem.Name = "SorgulamaToolStripMenuItem";
            this.SorgulamaToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.SorgulamaToolStripMenuItem.Text = "Sorgulama";
            // 
            // borçlandırToolStripMenuItem
            // 
            this.borçlandırToolStripMenuItem.Name = "borçlandırToolStripMenuItem";
            this.borçlandırToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.borçlandırToolStripMenuItem.Text = "Borçlandır";
            // 
            // YardımToolStripMenuItem
            // 
            this.YardımToolStripMenuItem.Name = "YardımToolStripMenuItem";
            this.YardımToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.YardımToolStripMenuItem.Text = "Yardım";
            // 
            // anaSayfa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 290);
            this.Controls.Add(this.MenuStrip1);
            this.IsMdiContainer = true;
            this.Name = "anaSayfa";
            this.Text = "Site Yönetim Programı";
            this.Load += new System.EventHandler(this.anaSayfa_Load);
            this.MenuStrip1.ResumeLayout(false);
            this.MenuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.MenuStrip MenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem DosyaToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem AyarlarToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem YönetimDeğişikliğiToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ÇıkışToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem KatSakinleriToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem KatMalikleriToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem OtoparkToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem AidatToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem AidatToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem AiadatEkleToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem AidatBilgilariToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem AidatDüzenlemeToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem GiderlerToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem GiderMakbuzuToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem GiderSorgulamaToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem GörevliToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem GörevliToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem YöneticiListesiToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem DönemToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem EkstreToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem MakbuzlarToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem AidatMakbuzuToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem GiderMakbuzuToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem FaturalarToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem KasaToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem BakiyeToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem SorgulamaToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem YardımToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borçlandırToolStripMenuItem;
    }
}

